/// <reference path="../../../typings/_reference.ts" />

module app.main {

    export interface Adult {
        min: number;
        max: number;
        default: number;
        countAsTraveller: boolean;
        canAccompanyMinors: boolean;
        countAsAdult: boolean;
        countAsChild: boolean;
        countAsInfant: boolean;
        isConcessionary: boolean;
    }

    export interface Child {
        min: number;
        max: number;
        default: number;
        countAsTraveller: boolean;
        canAccompanyMinors: boolean;
        countAsAdult: boolean;
        countAsChild: boolean;
        countAsInfant: boolean;
        isConcessionary: boolean;
    }

    export interface Infant {
        min: number;
        max: number;
        default: number;
        countAsTraveller: boolean;
        canAccompanyMinors: boolean;
        countAsAdult: boolean;
        countAsChild: boolean;
        countAsInfant: boolean;
        isConcessionary: boolean;
    }

    export interface Youth {
        min: number;
        max: number;
        default: number;
        countAsTraveller: boolean;
        canAccompanyMinors: boolean;
        countAsAdult: boolean;
        countAsChild: boolean;
        countAsInfant: boolean;
        isConcessionary: boolean;
    }

    export interface Student {
        min: number;
        max: number;
        default: number;
        countAsTraveller: boolean;
        canAccompanyMinors: boolean;
        countAsAdult: boolean;
        countAsChild: boolean;
        countAsInfant: boolean;
        isConcessionary: boolean;
    }

    export interface SeniorCitizen {
        min: number;
        max: number;
        default: number;
        countAsTraveller: boolean;
        canAccompanyMinors: boolean;
        countAsAdult: boolean;
        countAsChild: boolean;
        countAsInfant: boolean;
        isConcessionary: boolean;
    }

    export interface TravellerType {
        adult: Adult;
        child: Child;
        infant: Infant;
        youth: Youth;
        student: Student;
        seniorCitizen: SeniorCitizen;
    }

    export interface PassengerConfig {
        minTravellers: number;
        maxTravellers: number;
        maxChildrenPerGuardian: number;
        maxInfantsPerGuardian: number;
        maxChildrenAndInfantsPerGuardian: number;
        travellerTypes: TravellerType[];
        infantSelectionPromptText: string;
        infantSelectionPromptBasedOn: string;
    }

    export interface TravelRequestedDateWindow {
        availabilityLed: string;
        journeyTariffLed: string;
        datePriceLed: string;
        datePriceLedJourneyTariff: string;
        datePriceLedOneWay: string;
    }

    export interface TravelConfig {
        departureDestinationDisplayFormat: string[];
        periodToMinDepartDate: string;
        periodToMaxDepartDate: string;
        periodToMaxDepartDateRoundToMonthEnd: string;
        travelMaximumDateWindow: string;
        travelDurationToDefaultDepartDate: string;
        preferredDepartureReturnTime: string;
        travelRequestedDateWindow: TravelRequestedDateWindow;
    }

    export interface FareConditionConfig {
        serviceFareClassType: string[];
        defaultServiceClass: string;
        maxNoOfServiceClassSelections: number;
        searchOptions: string[];
        defaultJourneyTypes: string[];
        maxMultiCityPairs: number;
    }

    export interface DateConfig {
        departureReturnDateFormat: string;
    }

    export interface Route {
        // Airport.AGR: string[];
        // Airport.BOM: string[];
        // Airport.DEL: string[];
        // City.AGR: string[];
        // City.BOM: string[];
        // City.DEL: string[];
    }

    export interface TravelRoutesConfig {
        routes: Route[];
    }

    export interface ConfigModel {
        id: string;
        passengerConfig: PassengerConfig;
        travelConfig: TravelConfig;
        fareConditionConfig: FareConditionConfig;
        dateConfig: DateConfig;
        travelRoutesConfig: TravelRoutesConfig;
    }

}

