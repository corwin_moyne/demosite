/// <reference path="../../../typings/_reference.ts" />

module app.main {
    'use strict';


    interface IPassengerScope extends ng.IScope {
        passengerType: app.main.IPassenger;
        maxAllowedPassengers: number;
        
        init(): void;
        setPassengerTypeId(): void;
        setPassengerType(): void;
        setMaxAllowedPassengers(): void;
        increment(): void;
        decrement(): void;
        
        error: boolean;
        maxPassengerError: boolean;
    }
    
    interface IPassengerAttributes extends ng.IAttributes {
        passengerTypeId: number;
    }

    class PassengerDirective implements ng.IDirective {

        restrict = 'E';
        templateUrl = 'scripts/main/passenger-directive.html';
        replace = true;
        scope = {};

        constructor(private passengerFilterService: app.main.PassengerFilterService) { }

        link = (scope: IPassengerScope, element: ng.IAugmentedJQuery, attributes: IPassengerAttributes): void => {
            
            var passengerTypeId: number;
            
            scope.init = () => {
                scope.setPassengerTypeId();
                scope.setPassengerType();
                scope.setMaxAllowedPassengers();
            }
            
            scope.setPassengerTypeId = () => {
                passengerTypeId = attributes.passengerTypeId;
            }
            
            scope.setPassengerType = () => {
                scope.passengerType = this.passengerFilterService.getPassengerType(passengerTypeId);
            }
            
            scope.setMaxAllowedPassengers = () => {
                scope.maxAllowedPassengers = this.passengerFilterService.getMaxAllowedPassengers();
            }
            
            scope.increment = () => {
                if(this.passengerFilterService.isIncrementValid()) {
                    this.passengerFilterService.increment(passengerTypeId);
                    scope.maxPassengerError = false;
                }
                else {
                    scope.maxPassengerError = true;
                }
            }
            scope.decrement = () => {
                if(this.passengerFilterService.isDecrementValid(passengerTypeId)) {
                    this.passengerFilterService.decrement(passengerTypeId);
                }
            }  
            
            scope.init();
        }

        static factory(): ng.IDirectiveFactory {
            const directive = (passengerFilterService: app.main.PassengerFilterService) => new PassengerDirective(passengerFilterService);
            directive.$inject = ['app.main.PassengerFilterService'];
            return directive;
        }
    }

    angular.module('app.main').directive('passengerDirective', PassengerDirective.factory());
}