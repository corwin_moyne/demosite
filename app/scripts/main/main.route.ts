/// <reference path="../../../typings/_reference.ts" />

module app.main {
	'use strict';

	// Configure the routing for the application.
	angular.module('app.main')
		.config(routing);
		
	/**
	 * Function to setup routing for the module.
	 */
	routing.$inject = ['$stateProvider'];
	function routing(stateProvider: angular.ui.IStateProvider) {
		stateProvider
			.state('main', {
				url: '/main',
				templateUrl: 'scripts/main/main.html',
				controller: 'app.main.MainController',
				controllerAs: 'vm',
				resolve: {
					passengerIds: resolvePassengerIds,
					airports: resolveAirports
				}
			})
    }
	
	resolvePassengerIds.$inject = ['app.main.PassengerFilterService'];
	function resolvePassengerIds(passengerFilterService: app.main.PassengerFilterService): ng.IPromise<Array<number>> {
        return passengerFilterService.getPassengerTypesIds().then((response) => {
            return response.data;
        });
        // console.log('test', test);
		// return null;
	}
	
	resolveAirports.$inject = ['$http'];
	function resolveAirports($http: ng.IHttpService): ng.IPromise<any> {
		return $http.get('../../airports/airports.json').then((response) => {
			return response.data;
		});
	}
}