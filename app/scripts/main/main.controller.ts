/// <reference path="../../../typings/_reference.ts" />

module app.main {
    'use strict';

    class MainController {

        passengerTypeIds = new Array<number>();

        static $inject = [
            'app.main.PassengerFilterService',
            'passengerIds',
            'airports'
        ];
        constructor(
            private passengerFilterService: app.main.PassengerFilterService,
            public passengerIds,
            public airports) {
            
            console.log('ids', this.passengerIds)
        }
    }

    angular.module('app.main').controller('app.main.MainController', MainController);
}