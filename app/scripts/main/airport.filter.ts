/// <reference path="../../../typings/_reference.ts" />

module app.main {
	'use strict';
	
	export class AirportFilter {
		public static Factory() {
			return(input: any[], airportInput) => {
			
				var airportString: string;

				angular.forEach(input, (airport, key) => {
					if(airport.airportCode === airportInput) {
						airportString = airport.airportName + ', ' + airport.iata + ', ' + airport.city + ', ' + airport.country;
					}
				});
				return airportString;
			}
		}
	}
	
	angular.module('app.main').filter('airportfilter', [AirportFilter.Factory]);
}