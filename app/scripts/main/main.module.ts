/// <reference path="../../../typings/_reference.ts" />

module app.main {
  'use strict';

 var requiredModules: any[] = [];

  angular
    .module('app.main', requiredModules);
}
