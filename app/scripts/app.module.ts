/// <reference path="../../typings/_reference.ts" />

module app {
    'use strict';

    var requiredModules = [
        'ui.bootstrap',
        'ui.router',
        'ngAnimate',

        'app.main',
        'app.common'
    ];

    angular
        .module('app', requiredModules);
}
