/// <reference path="../../../typings/_reference.ts" />

module app.common {
    'use strict';

    var requiredModules: any[] = [];

    angular
        .module('app.common', requiredModules);
}
