/// <reference path="../../../typings/_reference.ts" />

module app.common {
    'use strict';

    export class SessionManagerService {

        static $inject = [
            '$http',
            'app.main.PassengerFilterService'
        ];
        constructor(
            private $http: ng.IHttpService, 
            private passengerFilterService: app.main.PassengerFilterService) {
        }

        init(): void {
            this.setPassengerData();
        }

        setPassengerData(): void {
            var config = this.getConfig().then((response) => {
                config = response
                this.passengerFilterService.init(config);
            }); 
        }

        getConfig(): ng.IPromise<any> {
            return this.$http.get('../../config/configuration.json').then((response: any) => {
                return response.data;
            });
        }
    }
    angular.module('app.common').service('app.common.SessionManagerService', SessionManagerService);
}