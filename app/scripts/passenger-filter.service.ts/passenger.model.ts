/// <reference path="../../../typings/_reference.ts" />

module app.main {
    'use strict';

    export interface IPassenger {
        passengerTypeId: number;
        passenger: string;
        min: number;
        qty: number;
    }

    export class Passenger implements IPassenger {
        static firstPassengerTypeId: number = 0;
        passengerTypeId: number;
        passenger: string;
        min: number;
        qty: number;

        constructor(passengerType: string) {
            this.passengerTypeId = Passenger.firstPassengerTypeId++;
            this.passenger = passengerType;
            passengerType === 'Adults' ? this.min = 1 : this.min = 0;
            this.qty = this.min;
        }
    }
}