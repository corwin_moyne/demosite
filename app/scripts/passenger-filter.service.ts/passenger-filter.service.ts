/// <reference path="../../../typings/_reference.ts" />

module app.main {
    'use strict';

    export class PassengerFilterService {

        private passengerTypes = new Array<app.main.IPassenger>();
        private passengerTypeIds: any;
        private maxAllowedTotalPassengers: number;
        private maxInfantAdultRatio: number;
        
        private passengerConfig: any = null;

        init(config): void {
            this.setPassengerConfig(config);
            this.setPassengerIds(config);
        }
        
        getPassengerTypeIds(): ng.IPromise<any> {
            return this.passengerTypeIds;
        }
        
        private setPassengerConfig(config): void {
            this.passengerConfig = config.passengerConfig;
        }
        
        private setPassengerIds(config: any): void {
            this.passengerTypeIds = Object.keys(this.passengerConfig.travellerTypes).map(Number);
        }
        
        
        
        
        
        
        
        
        getPassengerTypesIds(): number[] {
           console.log(this.passengerConfig)
            var array = Object.keys(this.passengerConfig.travellerTypes);
             console.log(this.passengerConfig);
            return null;        
        }

        getPassengerType(passengerTypeId: number): app.main.IPassenger {
            return this.passengerTypes[passengerTypeId];
        }

        getPassengerTypes(): app.main.IPassenger[] {
            return this.passengerTypes;
        }
        
        getMaxAllowedPassengers(): number {
            return this.maxAllowedTotalPassengers;
        }

        isIncrementValid(): boolean {
            var totalPassengers = this.getTotalPassengers();
            if (totalPassengers < this.maxAllowedTotalPassengers) {
                return true;
            }
            return false;
        }

        increment(passengerTypeId: number): void {
            this.passengerTypes[passengerTypeId].qty++;
        }

        isDecrementValid(passengerTypeId: number): boolean {
            var passenger = this.passengerTypes[passengerTypeId];
            if(passenger.qty > passenger.min) {
                return true;
            }
            return false;
        }
        
        decrement(passengerTypeId: number): void {
            this.passengerTypes[passengerTypeId].qty--;
        }

        getPassengerQty(passengerType: string): number {
            return;
        }

        private setPassengerTypes(config: any): void {
            angular.forEach(config.passengerConfig.travellerTypes, (value, key) => {
                this.passengerTypes.push(new app.main.Passenger(value.type));
            });
        }
        
        

        private setMaximumPassengers(config:any): void {
            this.maxAllowedTotalPassengers = config.passengerConfig.maxPassengers;
        }

        private setMaxInfantAdultRatio(config): void {
            this.maxInfantAdultRatio = config.passengerConfig.maxInfantsPerGuardian;
        }

        private getTotalPassengers(): number {
            var totalPassengers: number = 0;
            angular.forEach(this.passengerTypes, (value, key) => {
                totalPassengers += value.qty;
            });
            return totalPassengers;
        }
    }
    angular.module('app.main').service('app.main.PassengerFilterService', PassengerFilterService);
}