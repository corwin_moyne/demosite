///<reference path="../../typings/_reference.ts"/>

module app {
	'use strict';

    /**
     * Calls the run phase on the module.
     */
	angular.module('app')
        .run(run);

    /**
     * Defines the run function to be executed on the module.
     */
    run.$inject = ['app.common.SessionManagerService'];
    function run(sessionManagerService: app.common.SessionManagerService): void {
        sessionManagerService.init();
    }
}